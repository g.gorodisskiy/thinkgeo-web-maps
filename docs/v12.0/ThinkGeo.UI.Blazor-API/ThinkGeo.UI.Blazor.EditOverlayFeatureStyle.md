# EditOverlayFeatureStyle


## Inheritance Hierarchy

+ `Object`
  + **`EditOverlayFeatureStyle`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`EditOverlayFeatureStyle()`](#editoverlayfeaturestyle)|
|[`EditOverlayFeatureStyle(GeoColor,GeoColor,Int32)`](#editoverlayfeaturestylegeocolorgeocolorint32)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`FillColor`](#fillcolor)|[`GeoColor`](../ThinkGeo.Core/ThinkGeo.Core.GeoColor.md)|Gets or sets a color used for painting the inside (fill) area of a shape.|
|[`OutlineColor`](#outlinecolor)|[`GeoColor`](../ThinkGeo.Core/ThinkGeo.Core.GeoColor.md)|Gets or sets a color used for drawing the border of a shape.|
|[`OutlineWidth`](#outlinewidth)|`Int32`|Gets or sets an integer value that indicates the border width of a shape.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`EditOverlayFeatureStyle()`](#editoverlayfeaturestyle)|
|[`EditOverlayFeatureStyle(GeoColor,GeoColor,Int32)`](#editoverlayfeaturestylegeocolorgeocolorint32)|

### Protected Constructors


### Public Properties

#### `FillColor`

**Summary**

   *Gets or sets a color used for painting the inside (fill) area of a shape.*

**Remarks**

   *N/A*

**Return Value**

[`GeoColor`](../ThinkGeo.Core/ThinkGeo.Core.GeoColor.md)

---
#### `OutlineColor`

**Summary**

   *Gets or sets a color used for drawing the border of a shape.*

**Remarks**

   *N/A*

**Return Value**

[`GeoColor`](../ThinkGeo.Core/ThinkGeo.Core.GeoColor.md)

---
#### `OutlineWidth`

**Summary**

   *Gets or sets an integer value that indicates the border width of a shape.*

**Remarks**

   *N/A*

**Return Value**

`Int32`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


