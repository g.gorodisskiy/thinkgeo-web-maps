# ZoomBarLocation

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|UpperLeft|Upper left.|
|UpperRight|Upper right.|
|LowerLeft|Lower left.|
|LowerRight|Lower right.|

