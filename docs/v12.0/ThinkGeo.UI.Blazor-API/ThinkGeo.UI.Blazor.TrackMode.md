# TrackMode

**Summary**

N/A

**Remarks**

N/A

**Items**

|Name|Description|
|---|---|
|None|None.|
|Point|Point.|
|LineString|LineString.|
|Polygon|Polygon.|
|Circle|Circle.|
|Modify|Modify.|

