# SizeChangedMapViewEventArgs


## Inheritance Hierarchy

+ `Object`
  + `EventArgs`
    + **`SizeChangedMapViewEventArgs`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`SizeChangedMapViewEventArgs(Double,Double,Double,Double)`](#sizechangedmapvieweventargsdoubledoubledoubledouble)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`Height`](#height)|`Double`|Gets the height of map.|
|[`PreviousHeight`](#previousheight)|`Double`|Gets previous height of map.|
|[`PreviousWidth`](#previouswidth)|`Double`|Gets the previous width of map.|
|[`Width`](#width)|`Double`|Gets the width of map.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`SizeChangedMapViewEventArgs(Double,Double,Double,Double)`](#sizechangedmapvieweventargsdoubledoubledoubledouble)|

### Protected Constructors


### Public Properties

#### `Height`

**Summary**

   *Gets the height of map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `PreviousHeight`

**Summary**

   *Gets previous height of map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `PreviousWidth`

**Summary**

   *Gets the previous width of map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `Width`

**Summary**

   *Gets the width of map.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


